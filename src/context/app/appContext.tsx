import * as React from "react";
import {useContext, useEffect, useReducer} from "react";
import {Project, User} from "../../types";
import {reducerWithLogg} from "./appReducer";
import {projects} from "../../firebase/data-projects";
import {readProjects} from "../../firebase/repository/projectRepository";

export interface AppState {
    projects: Map<string, Project>;
    user?: User;
}

const appContext = React.createContext<{
    state: AppState;
    dispatch: Function;
}>({
    state: {projects: new Map<string, Project>()},
    dispatch: (state: AppState, action: any) => {
        return state;
    },
});

export const useAppContext = () => {
    return useContext(appContext);
};

export const AppProvider: React.FC = ({children}) => {
    const projectMap: Map<string, Project> = new Map();

    projects.forEach((project: Project) => projectMap.set(project.id, project));
    const initialState: AppState = {
        projects: projectMap,
    };

    const [state, dispatch] = useReducer(reducerWithLogg((state, action) => {
        console.log(action)
        console.log(state);
        return state;
    }), initialState);

    async function loadDataFromFirebase() {
        const projects = await readProjects();
        if (projects) {
            const projectMap = new Map(Object.entries(projects));
            dispatch({type: "PROJECTS_SET", payload: projectMap});
        }
    }

    useEffect(() => {
        loadDataFromFirebase();
    }, []);

    return (
        <appContext.Provider value={{state, dispatch}}>
            {children}
        </appContext.Provider>
    );
};
