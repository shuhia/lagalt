import {
  AccessForm,
  Comment,
  Member,
  Project,
  ProjectFormData,
  Roles,
  User,
  Vote,
} from "../../types";
import { AppState } from "./appContext";
import { v4 as uuidv4 } from "uuid";
import { deleteData } from "../../firebase/firebase";
import { saveProject } from "../../firebase/repository/projectRepository";

interface AppAction {
  type: any;
  payload: any | Vote;
}

export const reducer = (state: AppState, action: AppAction): AppState => {
  const { projects, user } = state;
  const { type, payload } = action;

  switch (type) {
    case "USER_LOGIN": {
      const newUser: User = {
        history: [],
        id: payload.id,
        name: payload.name,
        members: new Map(),
        votes: new Map(),
      };
      return { ...state, user: newUser };
    }
    case "USER_LOGOUT": {
      return { ...state, user: undefined };
    }
    case "PROJECTS_SET": {
      state.projects = payload;
      return { ...state };
    }
    case "PROJECT_JOIN": {
      const form: AccessForm = payload;
      if (user) {
        const member: Member = { ...form };
        const project = projects.get(form.projectId);
        if (project) {
          joinProjectAndUser(project, user, member);
          saveProject(project);
        }
      }

      return { ...state };
    }
    case "PROJECT_LEAVE": {
      const projectId: string = payload;
      if (user) {
        const project = projects.get(projectId);
        if (project) {
          project.members.delete(user.id);
          user.members.delete(project.id);
          saveProject(project);
        }
      }
      return { ...state };
    }
    case "PROJECT_CREATE": {
      if (user) {
        const projectFormData: ProjectFormData = payload;
        const projectId = uuidv4();
        const member: Member = {
          userId: user.id,
          role: Roles.owner,
          projectId,
        };
        const votes = new Map();
        const newProject: Project = {
          comments: new Map(),
          id: projectId,
          votes: votes,
          ...projectFormData,
        };
        projects.set(newProject.id, newProject);
        joinProjectAndUser(newProject, user, member);
        saveProject(newProject);
      }

      return { ...state };
    }
    case "PROJECT_UPDATE": {
      const updatedProject: Project = payload;
      projects.set(updatedProject.id, updatedProject);
      saveProject(updatedProject);
      return { ...state };
    }
    case "PROJECT_REMOVE": {
      const projectId = payload;
      projects.delete(projectId);
      deleteData(`projects/${projectId}`);
      return { ...state };
    }
    case "PROJECT_COMMENT_ADD": {
      const newComment = payload;
      const project = state.projects.get(newComment.projectId);
      if (project) {
        const comments = project.comments;
        // add comment
        comments.set(newComment.id, newComment);
        // id comment has a parent comment
        const mainComment = comments.get(newComment.commentId);
        if (mainComment)
          if (mainComment?.comments) {
            mainComment?.comments?.unshift(newComment.id);
          } else {
            mainComment["comments"] = [newComment.id];
          }
        return updateProject(state, project);
      }
      return { ...state };
    }
    case "PROJECT_COMMENT_REMOVE": {
      const comment: Comment = payload;
      const project = projects.get(comment.projectId);
      if (project) {
        const prev = project.comments.get(comment.id);
        if (prev) {
          prev.content = "removed";
          prev.isRemoved = true;
        }
        return updateProject(state, project);
      }
      return state;
    }

    case "PROJECT_COMMENT_EDIT": {
      const comment: Comment = payload;
      const project = projects.get(comment.projectId);
      if (project) {
        project.comments.set(comment.id, comment);
        return updateProject(state, project);
      }
      return state;
    }

    case "VOTE": {
      const vote: Vote = payload;
      const project = projects.get(vote.projectId);
      if (project && user) {
        const oldVote = project.votes.get(user.id);
        console.log(oldVote);
        if (oldVote) {
          if (oldVote.type === vote.type) {
            vote.type = 0;
          }
        }
        project.votes.set(vote.userId, vote);
        user.votes.set(project.id, vote);
        return updateProject(state, project);
      }

      return { ...state };
    }

    default:
      return state;
  }
};

export const reducerWithLogg =
  (callback?: (state: AppState, action: AppAction) => AppState) =>
  (state: AppState, action: AppAction) => {
    const newState = reducer(state, action);
    if (callback) callback(newState, action);
    return newState;
  };

function updateProject(state: AppState, project: Project): AppState {
  return reducer(state, { type: "PROJECT_UPDATE", payload: project });
}

function joinProjectAndUser(newProject: Project, user: User, member: Member) {
  newProject.members.set(member.userId, member);
  user.members.set(newProject.id, member);
}
