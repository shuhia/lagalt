import {saveData} from "./firebase";
import {projects} from "./data-projects";
import {mapPropsToObjProps} from "./util";

function initFirebaseStore() {
    projects.forEach((project) => {
        // Convert map properties to object
        const object = mapPropsToObjProps(project);
        saveData("/projects/" + project.id, object);
    });
}

export default initFirebaseStore;
