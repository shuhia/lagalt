import {Project, Vote} from "../types";

export const projects: Project[] = [
    {
        id: "1",
        title: "Jim Carrey and Metal music",
        type: "music",
        description:
            "Daniel Rossen is better known for his work with band Grizzly Bear, but unbeknownst to a lot of Grizzly's fans, he also works on his own solo work (his debut EP is well worth checking out) as well as under the name of Department of Eagles. This track is one of the best off the In Ear Park album and the album ranks right up there alongside the Grizzly Bear ones",
        comments: new Map(),
        members: new Map(),
        votes: new Map<string, Vote>(),
    },
    {
        id: "2",
        title: "Fornite",
        type: "game",
        description:
            "Fortnite is an online video game developed by Epic Games and released in 2017. It is available in three distinct game mode versions that otherwise share the same general gameplay and game engine: Fortnite Battle Royale, a free-to-play battle royale game in which up to 100 players fight to be the last person standing; Fortnite: Save the World, a cooperative hybrid tower defense-shooter and survival game in which up to four players fight off zombie-like creatures and defend objects with traps and fortifications they can build; and Fortnite Creative, in which players are given complete freedom to create worlds and battle arenas.",
        comments: new Map(),
        members: new Map(),
        votes: new Map<string, Vote>(),
    },
    {
        id: "3",
        title: "Inception",
        type: "film",
        description:
            "Cobb steals information from his targets by entering their dreams. Saito offers to wipe clean Cobb's criminal history as payment for performing an inception on his sick competitor's son.",
        comments: new Map(),
        members: new Map(),
        votes: new Map<string, Vote>(),
    },
    {
        id: "4",
        title: "Lagalt",
        type: "web",
        description:
            "This SRS (Software Requirements Specification) describes the software product to be created by one of the candidate groups in the Experis Academy in Sweden contingent of the Java Full-stack short course",
        comments: new Map(),
        members: new Map(),
        votes: new Map<string, Vote>(),
    },
];
