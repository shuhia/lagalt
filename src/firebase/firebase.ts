// Import the functions you need from the SDKs you need
import {FirebaseError, initializeApp} from "firebase/app";
import {get, getDatabase, ref, remove, set, update} from "firebase/database";
import {createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword, signOut, User,} from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyB17lVxUjKY8BzuRKtzD75CEfkaCzndG1I",
    authDomain: "lagalt-3dfb9.firebaseapp.com",
    projectId: "lagalt-3dfb9",
    storageBucket: "lagalt-3dfb9.appspot.com",
    databaseURL:
        "https://lagalt-3dfb9-default-rtdb.europe-west1.firebasedatabase.app",
    messagingSenderId: "437558366541",
    appId: "1:437558366541:web:0cc12268a5f92e6624a704",
    measurementId: "G-EJ2HPGFQWY",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
// Get a reference to the database service
export const database = getDatabase(app);

export const auth = getAuth(app);

export async function signup(email: string, password: string) {
    try {
        const userCredential = await createUserWithEmailAndPassword(
            auth,
            email,
            password
        );
        const user = userCredential.user;
        return user;
    } catch (error: any) {
        return handleError(error);
    }
}

export async function login(
    email: string,
    password: string
): Promise<User | void> {
    try {
        const userCredential = await signInWithEmailAndPassword(
            auth,
            email,
            password
        );
        const user = userCredential.user;
        return user;
    } catch (error: any) {
        return handleError(error);
    }
}

export async function logout() {
    try {
        await signOut(auth);
        return true;
    } catch (error: any) {
        return handleError(error);
    }
}

function handleError(error: FirebaseError) {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log(errorMessage);
    throw new Error(errorMessage);
}

export async function saveData(path: string, data: Object) {
    try {
        await set(ref(database, path), data);
        return true;
    } catch (error: any) {
        return handleError(error);
    }
}

export async function readData(path: string) {
    try {
        const data = await get(ref(database, path));
        return data;
    } catch (error: any) {
        return handleError(error);
    }
}

export async function updateData(path: string, data: Object) {
    try {
        await update(ref(database, path), data);
        return true;
    } catch (error: any) {
        return handleError(error);
    }
}

export async function deleteData(path: string) {
    try {
        await remove(ref(database, path));
        return true;
    } catch (error: any) {
        return handleError;
    }
}
