import {DataSnapshot} from "firebase/database";
import {Comment, Member, Roles} from "../types";
import * as firebase from "./firebase";
import * as projectRepository from "./repository/projectRepository";
import {ProjectImpl} from "./../types";

test("can save and read data", async () => {
    const id = "test1";
    await firebase.saveData("projects/" + id, {hello: "hello"});
    const data: DataSnapshot | void = await firebase.readData("projects");
    expect(data?.toJSON).toBeTruthy();
    await firebase.deleteData("projects/" + id);
});

test("can save and read data with members", async () => {
    const members = new Map();
    const id = "test2";
    const member: Member = {
        role: Roles.owner,
        userId: "1",
        projectId: id,
    };
    members.set("1", member);
    const project: ProjectImpl = {
        id: "test2",
        title: "Lagalt",
        type: "web",
        description:
            "This SRS (Software Requirements Specification) describes the software product to be created by one of the candidate groups in the Experis Academy in Sweden contingent of the Java Full-stack short course",
        comments: new Map(),
        members: members,
        votes: new Map(),
    };

    await projectRepository.saveProject(project);
    const data = await projectRepository.readProject(project.id);
    expect(data).toEqual(project);
    await firebase.deleteData("projects/" + project.id);
});

test("can save and read data with comments", async () => {
    const members = new Map();
    const id = "test2";
    const comments = new Map();
    const comment: Comment = {
        id: "testComment",
        userId: "",
        userName: "",
        projectId: id,
        content: "",
        createdAt: "",
    };
    comments.set(comment.id, comment);
    const project: ProjectImpl = {
        id: "test2",
        title: "Lagalt",
        type: "web",
        description:
            "This SRS (Software Requirements Specification) describes the software product to be created by one of the candidate groups in the Experis Academy in Sweden contingent of the Java Full-stack short course",
        comments: comments,
        members: members,
        votes: new Map(),
    };

    await projectRepository.saveProject(project);
    const data = await projectRepository.readProject(project.id);
    expect(data).toEqual(project);
    await firebase.deleteData("projects/" + project.id);
});
