export function mapPropsToObjProps(obj: Object): Object {
    const entries = Object.entries(obj).map(([key, value]) => {
        if (value instanceof Map) {
            const newProp = [key, mapPropsToObjProps(Object.fromEntries(value))];
            return newProp;
        } else return [key, value];
    });
    return Object.fromEntries(entries);
}

export function objPropsToMapProps<T = Object>(
    obj: T | Object,
    props: string[]
): T {
    const entries = Object.entries(obj).map(([key, value]) => {
        if (props.find((prop: string) => key === prop)) {
            const newProp = [key, new Map(Object.entries(value))];
            return newProp;
        } else return [key, value];
    });
    return Object.fromEntries(entries) as T;
}
