import {Project, ProjectImpl} from "../../types";
import {mapPropsToObjProps, objPropsToMapProps} from "../util";
import {deleteData, readData, saveData} from "../firebase";

const mapProps = ["members", "votes", "comments"];

export async function saveProject(project: Project) {
    const object = mapPropsToObjProps(project);
    return saveData(`projects/${project.id}`, object);
}

export async function readProject(id: string) {
    let data = await readData(`projects/${id}`);

    if (data) {
        let object = data.toJSON();
        if (object) {
            const project: ProjectImpl = objPropsToMapProps<ProjectImpl>(
                object,
                mapProps
            );
            const newProject = Object.assign(new ProjectImpl(), project);
            return newProject;
        }
    }
}

export async function readProjects() {
    const projectData = await readData("/projects");
    if (projectData) {
        const projectJSON = projectData.toJSON();
        if (projectJSON) {
            const entries = Object.entries(projectJSON).map(([key, value]) => {
                return [
                    key,
                    Object.assign(
                        new ProjectImpl(),
                        objPropsToMapProps<Project>(value, mapProps)
                    ),
                ];
            });
            const projects = Object.fromEntries(entries);
            return projects;
        }
    }
}

export async function deleteProject(id: string) {
    return deleteData("/project/" + id);
}
