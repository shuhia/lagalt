import {Stack} from "@mui/material";
import Protect from "../Authentication/Guard";
import PostForm from "../Project/Forms/FormTabs";
import ProjectListItem from "../Project/ProjectListItem";
import {Project} from "./../../types";
import {ProjectList} from "./../Project/ProjectList";

interface HomeProps {
    projects: Project[];
}

export function Home(props: HomeProps) {
    return (
        <Stack spacing={8}>
            <ProjectList>
                {props.projects?.map((project, index) => (
                    <ProjectListItem key={"project-" + index} project={project}/>
                ))}
            </ProjectList>
        </Stack>
    );
}
