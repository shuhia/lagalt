import {Typography} from "@mui/material";
import React from "react";
import {Link} from "react-router-dom";

function Error() {
    return (
        <>
            <Typography variant="h1">This page does not exist yet</Typography>
            <Link to="/">Back to home</Link>
        </>
    );
}

export default Error;
