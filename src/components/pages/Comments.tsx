import React from "react";
import {useAppContext} from "../../context/app/appContext";
import {CommentForm} from "../Project/Comment/CommentForm";
import ProjectListItem from "../Project/ProjectListItem";
import {CommentList} from "../Project/Comment/CommentList";
import Protect from "../Authentication/Guard";
import {useLocation} from "react-router-dom";

function Comments() {
    const {state} = useAppContext();
    const location = useLocation();
    const projectId = location.pathname.split("/").pop();
    if (projectId) {
        const {projects, user} = state;
        const project = projects.get(projectId);
        if (project) {
            const comments = project.comments;
            const topLevelCommentIds = Array.from(comments.values())
                .filter((comment) => comment?.commentId === "")
                .map((comment) => comment.id);

            return (
                <div>
                    <ProjectListItem project={project}></ProjectListItem>
                    <p>Comment as {user?.name}</p>
                    <Protect>
                        <CommentForm commentId="" projectId={project.id}></CommentForm>
                    </Protect>
                    <div className="comment-filter-provider">
                        <div className="comment-filter"></div>
                        <CommentList
                            commentIds={topLevelCommentIds}
                            project={project}
                        ></CommentList>
                    </div>
                </div>
            );
        }
    }
    return <div>Error! No project was found</div>;
}

export default Comments;
