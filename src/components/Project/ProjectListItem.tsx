import * as React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import { Button, Stack, Typography } from "@mui/material";

import { CommentsButton } from "./Comment/CommentsButton";
import Rating from "./Rating";
import { useAppContext } from "../../context/app/appContext";
import { Project, Vote } from "../../types";
import { RequestAccessModal } from "./Forms/AccessModalForm";
import { useNavigate } from "react-router-dom";

function BottomActionBar(props: any) {
  return (
    <Stack direction="row" spacing={1} alignItems="center">
      {props.children}
    </Stack>
  );
}

interface ProjectDetailsProps {
  title?: string;
  description?: string;
}

const Details: React.FC<ProjectDetailsProps> = (props) => {
  return (
    <>
      <Box sx={{ flex: 1 }}>
        <Typography variant="h5">{props.title}</Typography>
        <Typography variant="body1">{props.description}</Typography>
      </Box>
    </>
  );
};

function LeftActionBar(props: any) {
  return (
    <>
      <Stack
        sx={{ display: { xs: "none", sm: "block" }, marginBottom: "5px" }}
        justifyContent="center"
      >
        {props.children}
      </Stack>
    </>
  );
}

interface ListItemProps {
  project: Project;
}

export default function ProjectListItem(props: ListItemProps) {
  const project = props.project;
  const context = useAppContext();
  const navigate = useNavigate();
  const { state, dispatch } = context;
  const handleVote = (type: number) => {
    const user = state?.user;
    if (user) {
      const vote: Vote = {
        type,
        userId: user.id,
        projectId: project.id,
      };
      if (dispatch) dispatch({ type: "VOTE", payload: vote });
    }
  };

  const rating = Array.from(project.votes.values())
    .map((vote) => Number(vote.type))
    .reduce((sum: number, value: number) => sum + value, 0);

  const disabled = state?.user ? false : true;

  if (state?.user) {
    const user = state.user;
    const isMember = project.members.has(user.id);
    return (
      <Card
        sx={{ display: "flex", background: "lightgrey", minHeight: "200px" }}
      >
        <LeftActionBar>
          <Rating
            rating={rating}
            orientation="vertical"
            handleVote={handleVote}
            disabled={disabled}
          ></Rating>
        </LeftActionBar>

        <Box
          sx={{
            display: "flex",
            flex: "1",
            flexDirection: "column",
            background: "white",
            padding: 1,
          }}
        >
          <Details title={project.title} description={project.description} />
          <BottomActionBar projectId={project.id}>
            <Box sx={{ display: { xs: "block", sm: "none" } }}>
              <Rating
                rating={rating}
                orientation="horizontal"
                handleVote={handleVote}
                disabled={disabled}
              ></Rating>
            </Box>
            <CommentsButton project={project}></CommentsButton>
            <Box flexGrow="1"></Box>
            {!isMember ? (
              <RequestAccessModal
                userId={state.user.id}
                projectId={project.id}
              />
            ) : (
              <Button
                variant="contained"
                color="error"
                onClick={() => {
                  if (dispatch)
                    dispatch({ type: "PROJECT_LEAVE", payload: project.id });
                }}
              >
                Leave
              </Button>
            )}
          </BottomActionBar>
        </Box>
      </Card>
    );
  }

  return (
    <Card sx={{ display: "flex", background: "lightgrey", minHeight: "200px" }}>
      <LeftActionBar>
        <Rating
          rating={rating}
          orientation="vertical"
          handleVote={handleVote}
          disabled={disabled}
        ></Rating>
      </LeftActionBar>

      <Box
        sx={{
          display: "flex",
          flex: "1",
          flexDirection: "column",
          background: "white",
          padding: 1,
        }}
      >
        <Details
          title={project.title}
          description={project.description}
        ></Details>
        <BottomActionBar projectId={project.id}>
          <Box sx={{ display: { xs: "block", sm: "none" } }}>
            <Rating
              rating={rating}
              orientation="horizontal"
              handleVote={handleVote}
              disabled={disabled}
            ></Rating>
          </Box>
          <CommentsButton project={project}></CommentsButton>
          <Box flexGrow="1"></Box>
        </BottomActionBar>
      </Box>
    </Card>
  );
}
