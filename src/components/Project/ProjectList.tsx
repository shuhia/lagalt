import {Stack} from "@mui/material";
import React from "react";

export function ProjectList(props: any) {
    return <Stack spacing={2}>{props.children}</Stack>;
}
