import React from "react";
import {Project} from "../../types";
import ProjectListItem from "./ProjectListItem";

interface ProjectProps {
    project: Project;
}

function index(props: ProjectProps) {
    return (
        <div>
            <ProjectListItem project={props.project}></ProjectListItem>
        </div>
    );
}

export default index;
