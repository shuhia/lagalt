import React from "react";
import {Project} from "../../../types";
import {CommentListItem} from "./CommentListItem";

interface CommentListProps {
    commentIds: string[];
    project: Project;
}

export function CommentList(props: CommentListProps) {
    let commentIds: string[] = props.commentIds;
    if (commentIds instanceof Object) {
        commentIds = [...Object.values(commentIds)];
    }
    const project: Project = props.project;
    const comments = commentIds.map((id) => project.comments.get(id));

    return (
        <>
            {comments
                .map((comment) => {
                    if (comment) {
                        return (
                            <CommentListItem comment={comment}>
                                {comment?.comments && (
                                    <CommentList
                                        key={comment.id}
                                        commentIds={comment.comments}
                                        project={project}
                                    ></CommentList>
                                )}
                            </CommentListItem>
                        );
                    } else {
                        return <></>;
                    }
                })
                .sort((a, b) => {
                    return (
                        Date.parse(b.props.comment.createdAt) -
                        Date.parse(a.props.comment.createdAt)
                    );
                })}
        </>
    );
}
