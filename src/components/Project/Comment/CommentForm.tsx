import { Box, Button, FormControl, Stack, TextField } from "@mui/material";
import React, { useState } from "react";
import { useAppContext } from "../../../context/app/appContext";
import { Comment } from "../../../types";
import { v4 } from "uuid";

interface CommentFormProps {
  projectId: string;
  commentId?: string;
  onSubmit?: (comment: Comment) => void;
}

export function CommentForm(props: CommentFormProps) {
  const { projectId, commentId } = props;
  const { state, dispatch } = useAppContext();
  const { user } = state;
  const [content, setContent] = useState("");
  const submitComment = (comment: Comment) => {
    console.log(comment);

    dispatch({ type: "PROJECT_COMMENT_ADD", payload: comment });
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    // Construct comment
    if (user) {
      const comment: Comment = {
        id: v4(),
        commentId: commentId,
        userId: user.id,
        userName: user.name,
        projectId,
        content,
        comments: [],
        createdAt: new Date().toUTCString(),
      };
      submitComment(comment);
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      display="grid"
      gap={3}
      width="100%"
    >
      <div>
        <FormControl fullWidth={true}>
          <Stack>
            <TextField
              id="standard-multiline-static"
              multiline
              rows={4}
              placeholder="What are your thoughts?"
              required
              variant="outlined"
              name="comment"
              value={content}
              onChange={(e) => {
                setContent(e.target.value);
              }}
            />
            <Button type="submit" variant="contained">
              Comment
            </Button>
          </Stack>
        </FormControl>
      </div>
    </Box>
  );
}
