import { ChatBubbleOutline, CommentTwoTone } from "@mui/icons-material";
import {
  Avatar,
  Box,
  Button,
  dialogActionsClasses,
  Divider,
  Input,
  Stack,
} from "@mui/material";
import React, { useState } from "react";
import { useAppContext } from "../../../context/app/appContext";
import { Comment } from "../../../types";
import Protect from "../../Authentication/Guard";
import { CommentForm } from "./CommentForm";

const CustomDivider = () => (
  <Divider orientation="vertical" sx={{ margin: "0 20px" }}></Divider>
);

interface CommentListItemProps {
  comment: Comment;
  children?: any;
}

export function CommentListItem(props: CommentListItemProps) {
  const { user } = useAppContext().state;
  const { comment } = props;
  const { dispatch } = useAppContext();
  const [toggleForm, setToggleForm] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState(false);
  const [content, setContent] = useState(comment.content);

  return (
    <Stack direction="row" spacing={1} overflow="hidden">
      <Stack spacing={1} alignItems={"center"}>
        <Avatar></Avatar>
        <CustomDivider></CustomDivider>
      </Stack>

      <Stack spacing={1} width="100%">
        <Stack minHeight="40px" direction="row" alignItems={"center"}>
          Posted by {comment.userName} on {comment.createdAt}
        </Stack>
        <Stack spacing={1}>
          {isEdit ? (
            <p>
              <Input
                value={content}
                onChange={(e) => {
                  setContent(e.target.value);
                }}
              />
            </p>
          ) : (
            <p>{content}</p>
          )}
          <Stack direction="row">
            <Protect>
              <Button
                startIcon={<ChatBubbleOutline />}
                onClick={() => {
                  setToggleForm((prev: boolean) => !prev);
                }}
              >
                Reply
              </Button>
            </Protect>
            {user?.id === comment.userId && (
              <>
                {isEdit ? (
                  <>
                    <Button
                      color="success"
                      onClick={() => {
                        comment.content = content;
                        dispatch({
                          type: "PROJECT_COMMENT_EDIT",
                          payload: comment,
                        });

                        setIsEdit((prev) => !prev);
                      }}
                    >
                      Save
                    </Button>
                    <Button
                      color="error"
                      onClick={() => {
                        setIsEdit(false);
                        setContent(comment.content);
                      }}
                    >
                      Cancel
                    </Button>
                  </>
                ) : (
                  <Button
                    onClick={() => {
                      setIsEdit((prev) => !prev);
                    }}
                  >
                    Edit
                  </Button>
                )}
                <Box flex="1"></Box>
                <Button
                  color="error"
                  onClick={() => {
                    if (window.confirm("are you sure?"))
                      dispatch({
                        type: "PROJECT_COMMENT_REMOVE",
                        payload: comment,
                      });
                  }}
                >
                  Remove
                </Button>
              </>
            )}
          </Stack>
          <Stack direction="row">
            <Divider orientation="vertical" sx={{ margin: "0 20px" }}></Divider>
            {toggleForm && (
              <CommentForm
                projectId={comment.projectId}
                commentId={comment.id}
              ></CommentForm>
            )}
          </Stack>
          {props.children}
        </Stack>
      </Stack>
    </Stack>
  );
}
