import {CommentOutlined} from "@mui/icons-material";
import {Button} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {Project} from "../../../types";

export function CommentsButton(props: any) {
    const navigate = useNavigate();
    const project: Project = props.project;
    const navigateToComments = () => {
        navigate("/comments/" + project.id);
    };
    return (
        <Button
            variant="outlined"
            startIcon={<CommentOutlined/>}
            onClick={() => navigateToComments()}
        >
            {project?.comments?.size | 0} Comments
        </Button>
    );
}
