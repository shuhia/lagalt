import {
    Box,
    Button,
    Divider,
    FormControl,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    Tab,
    Tabs,
    TextField,
    Typography,
} from "@mui/material";
import React, {FormEvent} from "react";

import {ProjectFormData, ProjectType} from "../../../types";
import {useAppContext} from "../../../context/app/appContext";

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box sx={{p: 3}}>{children}</Box>}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

function BasicTabsForm(props: any) {
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    return (
        <Box sx={{width: "100%"}}>
            <Box sx={{borderBottom: 1, borderColor: "divider"}}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="basic tabs example"
                >
                    <Tab label="PROJECT" {...a11yProps(0)} />
                </Tabs>
            </Box>

            <TabPanel value={value} index={0}>
                <ProjectForm></ProjectForm>
            </TabPanel>
        </Box>
    );
}

function ProjectForm() {
    const {state, dispatch} = useAppContext();
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        const target = e.target as typeof e.target & {
            title: { value: string };
            type: { value: ProjectType };
            description: { value: string };
        };
        const formData: ProjectFormData = {
            title: target.title.value,
            type: target.type.value,
            description: target.description.value,
            members: new Map(),
        };
        if (dispatch) dispatch({type: "PROJECT_CREATE", payload: formData});
    };

    return (
        <Box component="form" onSubmit={handleSubmit} display="grid" gap={3}>
            <TextField
                InputLabelProps={{
                    shrink: true,
                }}
                fullWidth
                label="Title"
                variant="outlined"
                name="title"
            />
            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Project Type</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    label="Project type"
                    defaultValue={"music"}
                    fullWidth
                    name="type"
                >
                    {Object.keys(ProjectType).map((type, index) => (
                        <MenuItem key={index} value={type}>
                            {type}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <TextField
                InputLabelProps={{
                    shrink: true,
                }}
                multiline
                minRows={3}
                label="Description"
                placeholder="Text (optional)"
                name="description"
            ></TextField>
            <Divider component="div"/>
            <Button type="submit" variant="contained">
                Create
            </Button>
        </Box>
    );
}

function FormTabs() {
    return (
        <Box height="50vh">
            <Typography padding="10px" variant="h4">
                Create a Project
            </Typography>

            <Paper>
                <BasicTabsForm></BasicTabsForm>
            </Paper>
        </Box>
    );
}

export default FormTabs;
