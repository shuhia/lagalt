import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { ButtonGroup, Select } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import { SelectChangeEvent } from "@mui/material/Select";
import { AccessForm, Roles } from "../../../types";
import { useAppContext } from "../../../context/app/appContext";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function BasicModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}>Join</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
          ></Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}></Typography>
        </Box>
      </Modal>
    </div>
  );
}

interface AccessModalFormProps {
  userId: string;
  projectId: string;
}

export function RequestAccessModal(props: AccessModalFormProps) {
  const { state, dispatch } = useAppContext();
  const { userId, projectId } = props;
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [role, setRole] = React.useState<Roles>(Roles.developer);
  const handleChange = (event: SelectChangeEvent) => {
    setRole(event.target.value as Roles);
  };
  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log(role);
    const accessFormData: AccessForm = { projectId, role, userId };
    if (dispatch) dispatch({ type: "PROJECT_JOIN", payload: accessFormData });
  };
  return (
    <div>
      <Button variant="contained" onClick={handleOpen}>
        Request Access
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} component="form" onSubmit={handleSubmit}>
          <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Role</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={role}
                label="Role"
                onChange={handleChange}
              >
                {Object.values(Roles).map((role: string) => {
                  return (
                    <MenuItem key={role} value={role}>
                      {role}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>

            <ButtonGroup>
              <Button type="submit" variant="contained">
                Request Access
              </Button>

              <Button color="error" onClick={() => handleClose()}>
                Cancel
              </Button>
            </ButtonGroup>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
