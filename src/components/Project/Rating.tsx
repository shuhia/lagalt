import * as React from "react";

import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import {ArrowDownwardRounded, ArrowUpwardRounded} from "@mui/icons-material";
import {Box, Typography} from "@mui/material";

/**
 * Component will be controlled by by
 */

interface RatingProps {
    rating: number;
    orientation: any;
    handleVote: (type: number) => void;
    disabled?: boolean;
}

export default function Rating(props: RatingProps) {
    return (
        <Box sx={{margin: 1}}>
            <ToggleButtonGroup
                orientation={props.orientation}
                value={"up"}
                exclusive
                onChange={(event, value) => props.handleVote(value)}
                disabled={props.disabled}
            >
                <ToggleButton value={1} aria-label="up">
                    <ArrowUpwardRounded></ArrowUpwardRounded>
                </ToggleButton>
                <ToggleButton value={0} disabled>
                    <Typography
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                    >
                        {props.rating}
                    </Typography>
                </ToggleButton>
                <ToggleButton value={-1} aria-label="down">
                    <ArrowDownwardRounded></ArrowDownwardRounded>
                </ToggleButton>
            </ToggleButtonGroup>
        </Box>
    );
}
