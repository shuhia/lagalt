import * as React from "react";
import Toolbar from "@mui/material/Toolbar";
import {Stack} from "@mui/material";

export default function FilterBar(props: any) {
    return (
        <Toolbar sx={{background: "white"}}>
            <Stack direction="row" spacing={2}>
                {props.children}
            </Stack>
        </Toolbar>
    );
}
