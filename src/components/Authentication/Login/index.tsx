import * as React from "react";
import {useState} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import LoginForm from "./Form";
import {login} from "../../../firebase/firebase";
import {Alert} from "@mui/material";

const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

export default function Login(props: any) {
    const [open, setOpen] = useState(false);
    const [error, setError] = useState<any>(null);
    const handleError = (error: any) => {
        console.log(error);
        setError(error);
        setTimeout(() => {
            setError(null);
        }, 10000);
    };
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const handleLogin = async (user: any) => {
        try {
            const result = await login(user.email, user.password);
            if (result === null) {
                throw new Error("Failed to register");
            }
        } catch (error) {
            handleError(error);
        }
    };

    return (
        <>
            <Button variant="contained" onClick={handleOpen}>
                Login
            </Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {error && <Alert severity="error">{error.message}</Alert>}
                    <LoginForm handleLogin={handleLogin}></LoginForm>
                </Box>
            </Modal>
        </>
    );
}
