import React from "react";
import { useAppContext } from "../../context/app/appContext";
import { Navigate } from "react-router-dom";

const Protect = (props: any) => {
  const { state } = useAppContext();
  if (state?.user) {
    return <>{props.children}</>;
  } else if (props?.replacement) {
    return <>{props?.replacement}</>;
  } else {
    // Redirect user to error page
    return <></>;
  }
};

export default Protect;

export function ProtectAndRedirect(props: any) {
  const { state } = useAppContext();
  if (state?.user) {
    return <>{props.children}</>;
  } else {
    return <Navigate to="/error" replace></Navigate>;
  }
}
