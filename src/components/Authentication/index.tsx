import React, {useEffect} from "react";
import Login from "./Login";
import Logout from "./Logout";
import SignUp from "./SignUp";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../../firebase/firebase";
import {useAppContext} from "../../context/app/appContext";

function Authentication() {
    const [user, loading, error] = useAuthState(auth);

    const context = useAppContext();
    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((state) => {
            const dispatch = context.dispatch;
            if (dispatch) {
                if (auth.currentUser) {
                    dispatch({
                        type: "USER_LOGIN",
                        payload: {
                            id: auth.currentUser.uid,
                            name: auth.currentUser.email,
                        },
                    });
                } else if (auth.currentUser === null) {
                    dispatch({type: "USER_LOGOUT", payload: user});
                }
            }
            return () => unsubscribe();
        });
    }, []);
    if (user) {
        return (
            <>
                <Logout/>
            </>
        );
    }
    return (
        <>
            <Login/>
            <SignUp/>
        </>
    );
}

export default Authentication;
