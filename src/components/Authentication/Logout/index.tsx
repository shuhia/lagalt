import {Button} from "@mui/material";
import React from "react";
import {logout} from "../../../firebase/firebase";
import {useAppContext} from "../../../context/app/appContext";

function Logout(props: any) {
    const context = useAppContext();
    const {dispatch} = context;
    const handleLogout = () => {
        logout();
    };

    return (
        <>
            <Button variant="contained" onClick={handleLogout} color="error">
                Logout
            </Button>
        </>
    );
}

export default Logout;
