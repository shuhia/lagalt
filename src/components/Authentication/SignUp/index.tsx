import * as React from "react";
import {useState} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import SignUpForm from "./Form";
import {signup as signUp} from "../../../firebase/firebase";
import {Alert} from "@mui/material";

const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

export default function BasicModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const [error, setError] = useState<any>(null);
    const handleClose = () => setOpen(false);
    const handleError = (error: any) => {
        console.log(error);
        setError(error);
        setTimeout(() => {
            setError(null);
        }, 10000);
    };
    const handleSignUp = async (newUser: any) => {
        try {
            const user = await signUp(newUser.email, newUser.password);
            if (user === null) {
                throw new Error("User is null");
            }
            setOpen(false);
        } catch (error) {
            handleError(error);
        }
    };
    return (
        <>
            <Button variant="contained" onClick={handleOpen}>
                Sign Up
            </Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {error && <Alert severity="error">{error.message}</Alert>}
                    <SignUpForm handleSignUp={handleSignUp}></SignUpForm>
                </Box>
            </Modal>
        </>
    );
}
