import React from "react";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import {CssBaseline} from "@mui/material";
import {createRoot} from "react-dom/client";
import {AppProvider} from "./context/app/appContext";
import {BrowserRouter} from "react-router-dom";

const container = document.querySelector("#root");
// @ts-ignore
const root = createRoot(container);
root.render(
    <>
        <BrowserRouter>
            <CssBaseline/>
            <AppProvider>
                <App/>
            </AppProvider>
        </BrowserRouter>
    </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
