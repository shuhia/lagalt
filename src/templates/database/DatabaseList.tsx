import {ref} from "firebase/database";
import {useList} from "react-firebase-hooks/database";
import {database, saveData} from "../../firebase/firebase";
import React from "react";

const initialData = {
    users: {
        alovelace: {
            name: "Ada Lovelace",
            // Index Ada's groups in her profile
            groups: {
                // the value here doesn't matter, just that the key exists
                techpioneers: true,
                womentechmakers: true,
            },
        },
    },
};
export default function DatabaseList() {
    const [snapshots, loading, error] = useList(ref(database));
    saveData("/", initialData);
    return (
        <div>
            <p>
                {error && <strong>Error: {error}</strong>}
                {loading && <span>List: Loading...</span>}
                {!loading && snapshots && (
                    <React.Fragment>
            <span>
              List:{" "}
                {snapshots.map((v) => (
                    <>{JSON.stringify(v)}</>
                ))}
            </span>
                    </React.Fragment>
                )}
            </p>
        </div>
    );
}
