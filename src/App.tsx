import { Button, Container, ToggleButton } from "@mui/material";
import React, { useEffect, useState } from "react";
import "./App.css";
import Navbar from "./components/Nav/Navbar";
import { ProjectFilters } from "./types";
import FilterBar from "./components/Nav/Filterbar";
import { useAppContext } from "./context/app/appContext";
import Search from "./components/Search";
import { addResponseMessage, Widget } from "react-chat-widget";
import "react-chat-widget/lib/styles.css";
import Protect, { ProtectAndRedirect } from "./components/Authentication/Guard";
import { Route, Routes, useNavigate } from "react-router-dom";

import Error from "./components/pages/Error";
import pdf from "./Lagalt-Case.pdf";
import { Home } from "./components/pages/Home";
import { Submit } from "./components/pages/Submit";
import Comments from "./components/pages/Comments";

function App() {
  const context = useAppContext();
  const { state } = context;
  const [currentFilter, setCurrentFilter] = useState<string | null>(null);
  const [search, setSearch] = useState<string>("");

  const handleNewUserMessage = (newMessage: string) => {
    console.log(`New message incoming! ${newMessage}`);
  };
  useEffect(() => {
    addResponseMessage("Welcome to Lagalt!");
  }, []);
  const navigate = useNavigate();

  const handleNavigateSubmit = () => {
    navigate("/submit");
  };
  const handleNavigateAbout = () => {
    window.open(pdf);
  };

  const filteredProjects = Array.from(state.projects.values())
    .filter((project) => {
      if (currentFilter !== null) {
        if (currentFilter === "all") {
          return true;
        }
        return project.type === currentFilter;
      }
      return true;
    })
    .filter((project) => {
      const text = JSON.stringify(project).toLowerCase();
      return !!text.match(search.toLowerCase());
    });

  return (
    <div className="App">
      <Widget
        handleNewUserMessage={handleNewUserMessage}
        title="Lagalt"
        subtitle="Project"
      />
      <Navbar
        rightToolBar={
          <>
            <Button
              variant="contained"
              onClick={() => {
                navigate("/");
              }}
            >
              Home
            </Button>
            <Button variant="contained" onClick={handleNavigateAbout}>
              About
            </Button>
            <Protect>
              <Button
                onClick={handleNavigateSubmit}
                variant="contained"
                color="success"
              >
                New Project
              </Button>
            </Protect>
          </>
        }
        Search={
          <Search
            onSearch={(value: string) => {
              setSearch(value);
            }}
          />
        }
      >
        <FilterBar>
          {ProjectFilters.map((filter) => (
            <ToggleButton
              size="small"
              value={filter}
              selected={currentFilter === filter}
              key={filter}
              onClick={(_event, value) => {
                if (value !== currentFilter) setCurrentFilter(value);
                else setCurrentFilter(null);
              }}
            >
              {filter}
            </ToggleButton>
          ))}
        </FilterBar>
      </Navbar>

      <Container sx={{ marginTop: "20px" }}>
        <Routes>
          <Route
            path="/submit"
            element={<ProtectAndRedirect>{<Submit />}</ProtectAndRedirect>}
          />
          <Route path="/comments/*" element={<Comments></Comments>}></Route>
          <Route path="/" element={<Home projects={filteredProjects} />} />
          <Route path="/*" element={<Error />} />
        </Routes>
      </Container>
    </div>
  );
}

export default App;
