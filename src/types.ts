export enum ProjectType {
  music = "music",
  game = "game",
  film = "film",
  web = "web",
}

export interface Project {
  id: string;
  title: string;
  type: ProjectType | string;
  description: string;
  comments: Map<string, Comment>;
  members: Map<string, Member>;
  votes: Map<string, Vote>;
}

export class ProjectImpl implements Project {
  id: string = "";
  title: string = "";
  type: string = "";
  description: string = "";
  comments: Map<string, Comment> = new Map();
  members: Map<string, Member> = new Map();
  votes: Map<string, Vote> = new Map();
}

export interface ProjectFormData {
  title: string;
  type: ProjectType | string;
  description: string;
  members: Map<string, Member>;
}

export const ProjectFilters = ["all", ...Object.values(ProjectType)];

export interface Comment {
  id: string;
  commentId?: string | undefined;
  userId: string;
  userName: string;
  projectId: string;
  content: string;
  comments?: string[];
  createdAt: string;
  isRemoved?: boolean;
}

export class CommentImpl implements Comment {
  commentId: string = "";
  userName: string = "";
  id: string = "";
  userId: string = "";
  projectId: string = "";
  name = "";
  content: string = "";
  comments = [];
  createdAt = new Date().toUTCString();
  isRemoved = false;
}

export interface User {
  id: string;
  name: string;
  history: [];
  votes: Map<string, Vote>;
  members: Map<string, Member>;
}

export enum Roles {
  owner = "owner",
  maintainer = "maintainer",
  developer = "developer",
  reviewer = "reviewer",
}

export interface AccessForm {
  userId: string;
  projectId: string;
  role: Roles;
}

export interface Vote {
  projectId: string;
  type: number;
  userId: string;
}

export interface Member {
  role: Roles;
  userId: string;
  projectId: string;
}
